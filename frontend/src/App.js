import { Routes, Route } from "react-router-dom";

import { Navigation } from "./common";
import { Dashboard, Publishers, PublisherForm, Reports } from "./pages";

export default function App() {
  return (
    <section className="container">
      <Navigation title="Reports" theme="dark" />

      <Routes>
        <Route index path="/" element={<Dashboard />} />
        <Route path="publishers" element={<Publishers />} />
        <Route path="publishers/new" element={<PublisherForm />} />
        <Route path="reports" element={<Reports />} />
      </Routes>
    </section>
  );
}
