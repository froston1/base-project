export default function Loading({ grow = false }) {
  return (
    <div className={`spinner-${grow ? "grow" : "border"}`} role="status" />
  );
}
