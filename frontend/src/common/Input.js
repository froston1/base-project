export default function Input({
  type = "text",
  name,
  label,
  value,
  error,
  handleChange,
}) {
  return (
    <>
      <label htmlFor={name} className="form-label">
        {label}
      </label>
      <input
        type={type}
        className={`form-control ${error ? "is-invalid" : ""}`}
        id={name}
        value={value}
        onChange={(e) => handleChange(name, e.target.value)}
      />
      <div className="invalid-feedback">{error}</div>
    </>
  );
}
