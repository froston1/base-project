export default function Checkbox({ name, label, value, error, handleChange }) {
  console.log(value);
  return (
    <div className="form-check">
      <input
        id={name}
        name={name}
        className={`form-check-input ${error ? "is-invalid" : ""}`}
        type="checkbox"
        checked={value}
        onChange={(e) => handleChange(name, e.target.checked)}
      />
      <label className="form-check-label" htmlFor={name}>
        {label}
      </label>
      <div class="invalid-feedback">{error}</div>
    </div>
  );
}
