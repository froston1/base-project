import { Link } from "react-router-dom";

export default function NavigationLink({ title, to, active }) {
  return (
    <li className="nav-item">
      <Link className={`nav-link ${active ? "active" : ""}`} to={to}>
        {title}
      </Link>
    </li>
  );
}
