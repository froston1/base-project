import { Table } from "../common";

const monthNames = [
  "",
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export default function Report() {
  const columns = [
    { key: "id", label: "#" },
    {
      key: "month",
      label: "Mes",
      render: (row) => (
        <span className="badge bg-secondary">{monthNames[row.month]}</span>
      ),
    },
    { key: "year", label: "Year" },
  ];
  const data = [
    { id: 1, month: 1, year: 2023 },
    { id: 2, month: 2, year: 2023 },
  ];
  return <Table columns={columns} data={data} />;
}
