using Microsoft.AspNetCore.Mvc;
using MonthReports.Webapi.Models;

namespace MonthReports.Webapi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class PublishersController
{
    private PublishersRepository _publisherRepository;

    public PublishersController(PublishersRepository publishersRepository)
    {
        _publisherRepository = publishersRepository;
    }

    [HttpGet]
    public List<Publisher> GetPublishers([FromQuery]bool? isElder = null, [FromQuery]string? lastName = null) 
    {      

        var query = _publisherRepository.GetQuery();

        if(isElder != null)
        {
            query = query.Where(p => p.Elder == isElder);
        }

        if(lastName != null)
        {
            query = query.Where(p => p.LastName.ToLower() == lastName.ToLower());
        }

        var rv = query.ToList();


        return rv;
    }

    [HttpGet("count")]
    public int GetPublishersCount([FromQuery]bool? isElder = null, [FromQuery]string? lastName = null) 
    {      

        var query = _publisherRepository.GetQuery();

        if(isElder != null)
        {
            query = query.Where(p => p.Elder == isElder);
        }

        if(lastName != null)
        {
            query = query.Where(p => p.LastName.ToLower() == lastName.ToLower());
        }

        var rv = query.Count();

        return rv;
    }

}
