using MonthReports.Webapi.Models;

namespace MonthReports.Webapi.Services.Repositories
{
    public class GenericRepository<T> where T : BaseEntity
    {
        protected List<T> _internalList;

        public GenericRepository()
        {
            _internalList = new List<T>();
        }

        public IQueryable<T> GetQuery()
        {
            return _internalList.AsQueryable();
        }

        public void Add(T model)
        {

        }
    }
}