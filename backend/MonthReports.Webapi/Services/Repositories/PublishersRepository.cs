using MonthReports.Webapi.Models;
using MonthReports.Webapi.Services.Repositories;

public class PublishersRepository : GenericRepository<Publisher>
{
    public PublishersRepository()
    {
        _internalList = new List<Publisher> {
            new Publisher{
                FirstName = "Mateo",
                LastName = "Carlos",
                BirthDate = new DateTime(1980,01,01),
                Elder = true
            },
            new Publisher{
                FirstName = "Marcos",
                LastName = "Carlos",
                BirthDate = new DateTime(1980,01,01),
                Elder = false
            },
            new Publisher{
                FirstName = "Clara",
                LastName = "Perez",
                BirthDate = new DateTime(1980,01,01),
                Elder = false
            },
            new Publisher{
                FirstName = "Carolina",
                LastName = "Gonzalez",
                BirthDate = new DateTime(1980,01,01),
                Elder = false,
                RegularPioneer = true
            }
        };
    }
    
}